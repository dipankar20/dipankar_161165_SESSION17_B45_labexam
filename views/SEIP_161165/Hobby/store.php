<?php
require_once("../../../vendor/autoload.php");

use App\Hobby\Hobby;

foreach($_POST['hobby'] as $hobby){
	$objHobby = new Hobby();
	$objHobby->setUserName($_POST["userName"]);
	$objHobby->setHobby($hobby);
	$objHobby->store();
}
?>