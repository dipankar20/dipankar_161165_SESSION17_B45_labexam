<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
	session_start();
}
$msg = Message::getMessage();
echo "<div id='message' class='text-center'> $msg </div>";
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Profile Picture Create Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/bootstrap.min.css" media="all" />
	<style>
		.marginTop20px{
			margin-top: 20px;
		}
	</style>
</head>
<body>
	<div class="container marginTop20px">
	<div class="col-sm-4 col-md-offset-4 well">
	<form action="store.php" method="post" class="form-horizontal center" enctype="multipart/form-data">
	<h2 class="text-center">Add Profile Picture</h2>
	<div class="form-group">
	<label for="userName">Enter User Name:</label>
	<input type="text" name="userName" id="userName" placeholder="User Name">
	</div>
	<div class="form-group">
	<label for="profilePicture">Enter Profile Picture:</label>
	<input type="file" name="profilePicture" id="profilePicture" placeholder="Profile Picture">
	</div>
	<input type="submit" value="submit" class="btn btn-primary">
	</form>
	</div>
	</div>
<script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>
</body>
</html>