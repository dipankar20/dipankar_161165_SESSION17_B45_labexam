<?php
require_once("../../../vendor/autoload.php");

use App\Organization\Organization;

$objOrganization = new Organization();
$objOrganization->setData($_POST);
$objOrganization->store();
?>