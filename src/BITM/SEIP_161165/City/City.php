<?php
namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class City extends DB{
	private $cityName;
	
	public function setData($allPostData=null){
		if(array_key_exists("cityName",$allPostData)){
			$this->cityName = $allPostData["cityName"];
		}
	}
	
	public function store(){
		$arrayData = array($this->cityName);
		$query = "INSERT INTO city(city) VALUES(?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>