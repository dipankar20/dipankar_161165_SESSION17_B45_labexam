<?php
namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Gender extends DB{
	private $userName;
	private $gender;
	
	public function setData($allPostData=null){
		if(array_key_exists("userName",$allPostData)){
			$this->userName = $allPostData["userName"];
		}
		if(array_key_exists("gender",$allPostData)){
			$this->gender = $allPostData["gender"];
		}
	}
	
	public function store(){
		$arrayData = array($this->userName,$this->gender);
		$query = "INSERT INTO gender(userName,gender) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>