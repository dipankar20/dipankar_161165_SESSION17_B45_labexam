<?php
namespace App\Organization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Organization extends DB{
	private $organizationName;
	private $summary;
	
	public function setData($allPostData=null){
		if(array_key_exists("organizationName",$allPostData)){
			$this->organizationName = $allPostData["organizationName"];
		}
		if(array_key_exists("summary",$allPostData)){
			$this->summary = $allPostData["summary"];
		}
	}
	
	public function store(){
		$arrayData = array($this->organizationName,$this->summary);
		$query = "INSERT INTO organization_summary(organizationName,summary) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>