<?php
namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB{
	private $userName;
	private $birthday;
	
	public function setData($allPostData=null){
		if(array_key_exists("userName",$allPostData)){
			$this->userName = $allPostData["userName"];
		}
		if(array_key_exists("birthday",$allPostData)){
			$this->birthday = $allPostData["birthday"];
		}
	}
	
	public function store(){
		$arrayData = array($this->userName,$this->birthday);
		$query = "INSERT INTO  birthday(userName,birthDate) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>