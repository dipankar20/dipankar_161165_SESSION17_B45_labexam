<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class ProfilePicture extends DB{
	private $userName;
	private $profilePictureName;
	private $moveProfilePicture;
	
	public function setUserName($userName){
		$this->userName = $userName;
	}
	
	public function setProfilePictureName($profilePictureName){
		$this->profilePictureName = $profilePictureName;
	}
	
	public function setMoveProfilePicture($profilePictureTemporaryPath){
		$this->moveProfilePicture = $profilePictureTemporaryPath;
	}
	
	public function store(){
		$arrayData = array($this->userName,time().$this->profilePictureName);
		$query = "INSERT INTO profile_picture(userName,profilePictureName) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		$destination = "images/".time().$this->profilePictureName;
		$fileUploadResult = move_uploaded_file($this->moveProfilePicture,$destination);
		
		if($result && $fileUploadResult){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>