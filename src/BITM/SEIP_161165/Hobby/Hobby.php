<?php
namespace App\Hobby;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobby extends DB{
	private $userName;
	private $hobby;
	
	/*public function setData($allPostData=null){
		if(array_key_exists("userName",$allPostData)){
			$this->userName = $allPostData["userName"];
		}
		if(array_key_exists("hobby",$allPostData)){
			$this->hobby = $allPostData["hobby"];
		}
	}*/
	public function setUserName($userName){
		$this->userName = $userName;
	}
	
	public function setHobby($hobby){
		$this->hobby = $hobby;
	}
	
	public function store(){
		$arrayData = array($this->userName,$this->hobby);
		$query = "INSERT INTO hobby(userName,hobby) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>