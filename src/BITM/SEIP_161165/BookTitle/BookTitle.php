<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB{
	private $bookName;
	private $authorName;
	
	public function setData($allPostData=null){
		if(array_key_exists("bookName",$allPostData)){
			$this->bookName = $allPostData["bookName"];
		}
		if(array_key_exists("authorName",$allPostData)){
			$this->authorName = $allPostData["authorName"];
		}
	}
	
	public function store(){
		$arrayData = array($this->bookName,$this->authorName);
		$query = "INSERT INTO bookTitle(bookName,authorName) VALUES(?,?)";
		
		$STH = $this->DBH->prepare($query);
		$result = $STH->execute($arrayData);
		
		if($result){
			Message::setMessage("Success! Data has been stored successfully!");
		} else{
			Message::setMessage("OOPS! Data has not been stored!");
		}
		Utility::redirect("create.php");
	}
}
?>